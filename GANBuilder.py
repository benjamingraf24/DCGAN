from keras.layers import Dropout,Dense, Input,Activation,UpSampling2D,Conv2DTranspose,BatchNormalization,LeakyReLU,Reshape,Conv2D,Flatten
from keras.models import Model, Sequential
from keras.optimizers import Adam,RMSprop

class GANBuilder:
	def __init__(self,rows,cols,noise_length):
		self.rows=rows
		self.cols=cols
		self.noise_length=noise_length
		self.genModel=self.createGenerator()
		self.disModel=self.createDiscriminator()
		self.stackedModel=self.createStackedModel()


		opt = Adam()
		self.stackedModel.compile(loss='binary_crossentropy', optimizer=opt, metrics=['accuracy'])

		dis_opt = RMSprop(lr=0.0001, decay=3e-8)
		self.disModel.compile(loss='binary_crossentropy', optimizer=dis_opt, metrics=['accuracy'])

	def createGenerator(self):
		noise_shape = self.noise_length
		self.G = Sequential()
		dropout = 0.2
		depth = 256
		dim = 11

		self.G.add(Dense(dim*dim*depth, input_dim=(noise_shape)))	
		self.G.add(BatchNormalization(momentum=0.9))
		self.G.add(LeakyReLU(0.2))
		self.G.add(Reshape((dim, dim, depth)))

		self.G.add(UpSampling2D())
		self.G.add(Conv2D(128, (3,3), padding='same'))
		self.G.add(BatchNormalization(momentum=0.9))
		self.G.add(LeakyReLU(0.2))

		self.G.add(UpSampling2D())
		self.G.add(Conv2D(64, (3,3), padding='same'))
		self.G.add(BatchNormalization(momentum=0.9))
		self.G.add(LeakyReLU(0.2))

		self.G.add(UpSampling2D())
		self.G.add(Conv2D(32, (3,3), padding='same'))
		self.G.add(BatchNormalization(momentum=0.9))
		self.G.add(LeakyReLU(0.2))

		self.G.add(UpSampling2D())
		self.G.add(Conv2D(3, (3,3), padding='same'))
		self.G.add(BatchNormalization(momentum=0.9))
		self.G.add(Activation('sigmoid'))

		return self.G


	def createDiscriminator(self):

	
		self.D = Sequential()
		depth = 64
		dropout = 0.2
		# In: 28 x 28 x 1, depth = 1
		# Out: 14 x 14 x 1, depth=64
		input_shape = (self.rows, self.cols, 3)
		self.D.add(Conv2D(64, 3, strides=2, input_shape=input_shape,padding='same'))
		self.D.add(BatchNormalization(momentum=0.5))
		self.D.add(LeakyReLU(0.2))


		self.D.add(Conv2D(64, 3, strides=2, input_shape=input_shape,padding='same'))
		self.D.add(BatchNormalization(momentum=0.5))
		self.D.add(LeakyReLU(0.2))

		self.D.add(Conv2D(128, 3, strides=2, padding='same'))
		self.D.add(BatchNormalization(momentum=0.5))
		self.D.add(LeakyReLU(0.2))

		self.D.add(Conv2D(256, 3, padding='same'))
		self.D.add(BatchNormalization(momentum=0.5))
		self.D.add(LeakyReLU(0.2))


		# Out: 1-dim probability
		self.D.add(Flatten())
		self.D.add(Dense(1))
		self.D.add(Activation('sigmoid'))
				# For the combined model we will only train the generator
		return self.D



	def createStackedModel(self):
		# Build stacked GAN model
		self.AM = Sequential()
		self.AM.add(self.genModel)
		self.AM.add(self.disModel)
		self.AM.summary()
		return self.AM


	def makeDisTrainable(self,arg):
		self.disModel.trainable = arg
		for l in self.disModel.layers:
			l.trainable = arg



