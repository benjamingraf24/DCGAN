
import numpy as np
import time
from tensorflow.examples.tutorials.mnist import input_data

from keras.models import Sequential
from keras.layers import Dense, Activation, Flatten, Reshape
from keras.layers import Conv2D, Conv2DTranspose, UpSampling2D
from keras.layers import LeakyReLU, Dropout
from keras.layers import BatchNormalization
from keras.optimizers import Adam, RMSprop
from GANBuilder import GANBuilder

import matplotlib.pyplot as plt
from keras import backend as K




if __name__ == '__main__':

	picture_rows=176
	picture_cols=176
	gen_noise_length=100

	iterations=10
	batch_size=64

	save_interval=200

	experiment_filepath="Result/"

	#Load training data
	train_data=np.load('pictures.npy').astype(np.float32)[:,4:,4:] #schange from 180x180 to 176x176


	#Build GAN
	GAN = GANBuilder(picture_rows,picture_cols,gen_noise_length)

	plt.ion()
	for t in range(iterations):

		#Pick random batch
		images_train = train_data[np.random.randint(0,train_data.shape[0], size=batch_size), :, :, :]

		#Generate Fake Images
		noise = np.random.uniform(-1.0, 1.0, size=[batch_size, gen_noise_length])
		images_fake = GAN.genModel.predict(noise)

		plt.clf()
		plt.imshow(images_fake[0,:,:,:])
		plt.pause(0.1)

		#Build training data for Discriminator
		x = np.concatenate((images_train, images_fake))
		y = np.ones([2*batch_size, 1])
		y[batch_size:, :] = 0

		#Train DIS
		GAN.makeDisTrainable(True)
		#d_loss = GAN.disModel.test_on_batch(x, y)
		d_loss=GAN.disModel.train_on_batch(x, y)


		#Train Gen

		y = np.ones([batch_size, 1])
		noise = np.random.uniform(-1.0, 1.0, size=[batch_size, gen_noise_length])
		GAN.makeDisTrainable(False)
		#g_loss = GAN.stackedModel.test_on_batch(noise, y)
		g_loss=GAN.stackedModel.train_on_batch(noise, y)




		#Print Metrics
		log_mesg = "%d: [D loss: %f, acc: %f]" % (t, d_loss[0], d_loss[1])
		log_mesg = "%s  [G loss: %f, acc: %f]" % (log_mesg, g_loss[0], g_loss[1])
		print(log_mesg)


		'''
		if (t)%save_interval==0:
			
			filename = "Generated_%d.png" % t
			noise = np.random.uniform(-1.0, 1.0, size=[16, 1000])
			images = GAN.genModel.predict(noise)
			plt.figure(figsize=(10,10))
			for i in range(images.shape[0]):
				plt.subplot(4, 4, i+1)
				image = images[i, :, :, :]
				image = np.reshape(image, [picture_rows, picture_cols,3])
				plt.imshow(image)
				plt.axis('off')
			plt.tight_layout()
			plt.savefig(filename)
			plt.close('all')
			

			with open(experiment_filepath+"stacked_model.json", 'w') as f:
				f.write(GAN.stackedModel.to_json())
				GAN.stackedModel.save_weights(experiment_filepath+"stacked_model_weights_%d.hdf5" % t)
			with open(experiment_filepath+"dis_model.json", 'w') as f:
				f.write(GAN.disModel.to_json())
				GAN.disModel.save_weights(experiment_filepath+"dis_model_weights_%d.hdf5" % t)
			with open(experiment_filepath+"gen_model.json", 'w') as f:
				f.write(GAN.genModel.to_json())
				GAN.genModel.save_weights(experiment_filepath+"gen_model_weights_%d.hdf5" % t)

		'''

	#Generate Fake Images
	noise = np.random.uniform(-1.0, 1.0, size=[batch_size, gen_noise_length])
	images_fake = GAN.genModel.predict(noise)
	images_train = train_data[np.random.randint(0,train_data.shape[0], size=batch_size), :, :, :]
	y = np.ones([batch_size, 1])
	pred=GAN.stackedModel.train_on_batch(noise,y)
	print(pred)

